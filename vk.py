#! /usr/bin/env python3
import requests
import time
import logging
from enum import IntEnum

__vk_api_version__ = '5.68'
logger = logging.getLogger(__name__)


class VKException(Exception):
    pass


class LimitReachedError(VKException):
    pass


def get_access_token():
    pass


class VKError(IntEnum):
    TooManyRequestsPerSecond = 6
    InternalServerError = 10


class VKBase:
    base_url = 'https://api.vk.com/method/{method}'

    def __init__(self, token=None):
        self._token = token

    def api_call(self, method, params=None, sleep_time=0.5):
        url = self.base_url.format(method=method)
        params = params or {}
        params.update({'access_token': self._token})
        resp = requests.get(url, params=params)
        result = resp.json()

        if 'error' in result:
            # Too many requests per second
            err_code = result['error'].get('error_code')
            if err_code == VKError.TooManyRequestsPerSecond:
                logger.warning("Too many requests per second. Sleep {} "
                               "second(s) and try again.".format(sleep_time))
                time.sleep(sleep_time)
                return self.api_call(method, params)
            elif err_code == VKError.InternalServerError:
                logger.warning("Internal server error . Sleep {} second(s) "
                               "and try again.".format(sleep_time * 10))
                time.sleep(sleep_time * 10)
                return self.api_call(method, params)
            else:
                logger.critical(result, exc_info=True)
                raise VKException(result)
        return result['response']


class Users(VKBase):
    def __init__(self, token=None):
        super().__init__(token)

    def get(self, user_ids):
        """ Returns detailed information on users. """
        if isinstance(user_ids, int):
            user_ids = [user_ids]
        user_ids_iter = map(str, user_ids)
        params = {"user_ids": ','.join(user_ids_iter)}
        return self.api_call('users.get', params)


class Messages(VKBase):
    def __init__(self, token=None):
        super().__init__(token)

    def get(self):
        """ Returns a list of the current user's incoming or outgoing private
            messages.
        """
        params = {}
        return self.api_call('messages.get', params)

    def get_by_id(self, message_ids, preview_length=0):
        """ Returns messages by their IDs. """
        pass

    def get_chat(self, chat_id):
        """ Returns information about a chat. """
        params = {'chat_id': chat_id}
        return self.api_call('messages.getChat', params)

    def get_chat_users(self, chat_id):
        """ Returns a list of IDs of users participating in a chat. """
        params = {'chat_id': chat_id}
        return self.api_call('messages.getChatUsers', params)

    def get_dialogs(self):
        """ Returns a list of the current user's conversations. """
        return self.api_call('messages.getDialogs')

    def get_history(self, chat_id, offset=0, start_message_id=0, rev=0):
        """ Returns message history for the specified user or group chat. """
        params = {
            'chat_id': chat_id,
            'offset': offset,
            'start_message_id': start_message_id,
            'rev': rev}
        return self.api_call('messages.getHistory', params)

    def get_history_attachments(self, chat_id):
        """ Returns media files from the dialog or group chat. """
        pass

    def send(self, chat_id, message):
        """ Sends a message. """
        params = {
            'chat_id': chat_id,
            'message': message}
        return self.api_call('messages.send', params)


class VK(VKBase):
    def __init__(self, token=None):
        super().__init__(token)
        self.users = Users(token)
        self.messages = Messages(token)
        self._users_cache = {}



if __name__ == '__main__':
    pass

