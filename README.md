## Description
vk2tg.py is a simple messages forwarder from vk.com group chat to telegram chat.

As an addition it has sender.py service which can send messages back from telegram 
to vk.com via /send command.



## Usage

## Dependencies
service needs telepot raven requests pyyaml packages to work.  

You can install them with:  
```pip3 install -r requirements.txt```

## Contribution

## License 
This software is distributed under MIT license.