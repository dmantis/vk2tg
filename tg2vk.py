#!/usr/bin/env python3

import yaml
import os
import telepot
import time
import logging
from telepot.loop import MessageLoop
from raven.handlers.logging import SentryHandler
from raven.conf import setup_logging
from vk import VK

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(current_dir)
with open('setting.yaml') as f:
    setting = yaml.safe_load(f)

log_path = setting.get('sender_log')
if log_path:
    fh = logging.FileHandler(log_path)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

sentry_url = setting.get('sentry_url_sender')
if sentry_url:
    sentry_handler = SentryHandler(sentry_url)
    sentry_handler.setLevel(logging.ERROR)
    setup_logging(sentry_handler)


tg_chat_id = setting['telegram']['chat_id']
vk_chat_id = setting['vk']['chat_id']
vk_token = setting['vk']['access_token']

vk = VK(vk_token)


def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    logger.info("received {0} {1} {2}".format(
        content_type, chat_type, chat_id))
    
    #  logger.info("{0} {1}: {2}".format(msg['from']['first_name'], msg['from']['last_name'], msg['text']))
    if content_type == 'text' and chat_id == tg_chat_id:
        if msg['text'].startswith('/send '):
            first_name = msg['from'].get('first_name', '')
            last_name = msg['from'].get('last_name', '')
            print(first_name)
            vk_msg = "[{first_name} {last_name}] {msg}".format(
                first_name=first_name, 
                last_name=last_name, 
                msg=msg['text'][6:])
            vk.messages.send(vk_chat_id, vk_msg)
            logger.info("send {0} to vk chat {1}".format(vk_msg, vk_chat_id))


if __name__ == '__main__':
    logger.info("sender service started")
    bot = telepot.Bot(setting['telegram']['api_key'])
    MessageLoop(bot, handle).run_as_thread()

    while True:
        time.sleep(1)
