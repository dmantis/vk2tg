#! /usr/bin/env python3
import yaml
import os
import telepot
import logging
import time
import cgi
from raven.handlers.logging import SentryHandler
from raven.conf import setup_logging
from telepot.helper import Sender

from vk import VK, VKException

current_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(current_dir)
with open('setting.yaml') as f:
    setting = yaml.safe_load(f)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

log_path = setting.get('log')
if log_path:
    fh = logging.FileHandler(log_path)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

sentry_url = setting.get('sentry_url')
if sentry_url:
    sentry_handler = SentryHandler(sentry_url)
    sentry_handler.setLevel(logging.ERROR)
    setup_logging(sentry_handler)


def save_current_message_id(mid):
    with open('mid.cache', 'w') as f:
        f.write(str(mid))


def load_current_message_id():
    if not os.path.exists('mid.cache'):
        logger.warning("mid.cache file not found")
        return 0
    with open('mid.cache', 'r') as f:
        mid = f.read()
    if not mid.isdigit():
        logger.error("incorrect message id cache value: {}".format(mid))
        return 0
    return int(mid)


class VK2Telegram(VK):
    def __init__(self, token, tg_key, tg_chat_id):
        super().__init__(token)
        self._users_cache = {}
        self.bot = telepot.Bot(tg_key)
        self.tg_sender = Sender(self.bot, tg_chat_id)

    def load_chat_users(self, chat_id):
        user_ids = self.messages.get_chat_users(chat_id)
        users = self.users.get(user_ids)
        for user in users:
            self._users_cache[user['uid']] = user
            self._users_cache[user['uid']].pop('uid')
        logger.info("{} users load to cache".format(len(self._users_cache)))
        logger.debug("cached users: {}".format(self._users_cache))

    def get_user_by_uid(self, uid):
        """ Wrapper around users.get with memory cache. """
        if uid not in self._users_cache:
            user = self.users.get(uid)[0]
            self._users_cache[uid] = user if user else {}
        first_name = self._users_cache[uid].get('first_name')
        last_name = self._users_cache[uid].get('last_name')
        return first_name, last_name

    def message_listen_loop(self, chat_id, start_message_id=0,
                            sleep_time=1):
        current_mid = start_message_id

        while True:
            time.sleep(sleep_time)

            # bad style, but works
            try:
                new_messages = self.messages.get_history(
                    chat_id, start_message_id=current_mid)
            except (VKException, ConnectionError) as err:
                logger.error(err, exc_info=True)
                time.sleep(60)
                continue

            new_messages.reverse()

            if len(new_messages) <= 1:
                continue

            for msg in new_messages[:-1]:
                self.process_message(msg)
                current_mid = msg.get('mid')
                if isinstance(current_mid, int):
                    save_current_message_id(current_mid)

    def process_message(self, msg, fwd=False):
        logger.debug("process msg: {}".format(msg))
        first_name, last_name = self.get_user_by_uid(msg['uid'])
        tg_msg_template = "<b>{first_name} {last_name}</b>: {body}"
        tg_msg = tg_msg_template.format(first_name=first_name,
                                        last_name=last_name,
                                        body=cgi.escape(msg['body']))
        if fwd:
            tg_msg = '<b>[Fwd]</b> ' + tg_msg

        self.tg_sender.sendMessage(tg_msg, parse_mode='HTML')
        attachments = msg.get('attachments')
        for attach in attachments or []:
            self.process_attachment(attach)
        fwd_messages = msg.get('fwd_messages')
        for fwd_message in fwd_messages or []:
            self.process_message(fwd_message, fwd=True)

    def process_attachment(self, attach):
        logger.debug("process attach {}".format(attach))
        if attach['type'] == 'photo':
            self.tg_sender.sendPhoto(photo=attach['photo']['src_big'])
        elif attach['type'] == 'doc':
            title = attach['doc'].get('title', '') + ' (vk link)'
            #  self.tg_sender.sendDocument(document=attach['doc']['url'],
            #                              caption=caption)
            doc_data = '<a href="{link}">{title}</a>'.format(
                link=attach['doc']['url'], title=cgi.escape(title))
            self.tg_sender.sendMessage(doc_data, parse_mode='HTML')
        else:
            logger.warning("Unknown attachment type: {}".format(attach))


if __name__ == '__main__':
    logger.info("Service started")

    tg_chat_id = setting['telegram']['chat_id']
    vk_chat_id = setting['vk']['chat_id']

    vk2tg = VK2Telegram(
        token=setting['vk']['access_token'],
        tg_key=setting['telegram']['api_key'],
        tg_chat_id=setting['telegram']['chat_id']
    )

    vk2tg.load_chat_users(vk_chat_id)
    start_mid = load_current_message_id()
    vk2tg.message_listen_loop(chat_id=vk_chat_id, start_message_id=start_mid)
